{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

-- |
-- Module      : LiBar.Types
-- Description : Types used by other LiBar modules
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module contains most of the types for LiBar
module LiBar.Types where

import Control.Monad.Reader
import Data.Map (Map)
import Data.Text
import Reflex.Host.Headless (MonadHeadlessApp)


type LiBarMonad m t c = (MonadReader c m, MonadIO m, MonadHeadlessApp t m)


-- | A Displayer takes a value and returns a string modifier.  This
-- setup is used so that multiple different effects can be performed
-- on a single value while all being dependent on the original value.
-- For example, a cpu load value might want to both turn into a
-- vertical bar based on the load and also change color above a
-- certain threshold.
type Displayer a = a -> Text -> Text

-- | A class for turning values into something that can be displayed
-- in the bar.  For most classes, the default instance will work, but
-- text based classes (i.e. String, Text) will be included exactly
-- without any encoding marks (e.g. newlines with remain newlines
-- instead of \n).
class Dispable a where
  display :: a -> Text
  default display :: Show a => a -> Text
  display = pack . show

instance Dispable Text where
  display = id

instance Dispable Double

instance Dispable Int

instance Dispable Integer

instance Dispable [Double]

instance Dispable [Int]

instance Dispable [Text]

instance (Show a, Show b) => Dispable (Map a b)

instance (Dispable a, Dispable b) => Dispable (a, b) where
  display (x, y) = display x <> " " <> display y

instance (Dispable a) => Dispable (Maybe a) where
  display Nothing = ""
  display (Just x) = display x

-- | A foreground and background color pair.  Values of Nothing mean
-- that the pre-existing value should remain.  This will probably be
-- replaced with something better down the line.  Hopefully soon.
type Color = (Maybe Text, Maybe Text)

-- | A set of colors for a bar
data ColorScheme = ColorScheme
  { -- | The standard text color
    csDefaultColor :: Color,
    -- | Available colors for highlighted text
    csHighlights :: [Color]
  }
