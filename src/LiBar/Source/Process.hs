-- |
-- Module      : LiBar.Source.Process
-- Description : A simple source to get data from system processes
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This modules exports sources for easily retrieving values from
-- system processes.  One objective of LiBar is to be self-contained,
-- so as not to be constantly starting processes all the time, but it
-- can be useful from time to time.
--
-- The particular use case for this was password access.  I wanted a
-- simple action where the user could supply a command that would
-- return a password from their password manager so that it could be
-- used by the rest of the system.
module LiBar.Source.Process (process) where

import Data.Text
import System.Process

-- | Run a system command and get the program's output
process :: Text -> IO Text
process cmd = do
  (_, key, _) <-
    readCreateProcessWithExitCode
      (shell $ unpack cmd)
      ""
  return $ pack key
