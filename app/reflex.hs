{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Reader
import Control.Monad.Reader.Class ()
import Data.Default
import Data.List (sortOn)
import Data.List.Extra (groupOn)
import qualified Data.Map as M
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import LiBar
import LiBar.Formatter
import LiBar.Sink.LemonBar
import LiBar.Source.CPU
import LiBar.Source.GH
import LiBar.Source.Mem
import LiBar.Source.Net
import LiBar.Source.Process
import LiBar.Source.Time
import LiBar.Source.Waka
import LiBar.Types
import Reflex

selector :: Int -> [Text] -> Text
selector counter lst = l !! (counter `rem` length l)
  where
    l = filter (\x -> T.length x > 0) lst

showPad :: (Show a) => Char -> Int -> a -> Text
showPad c n x =
  let result = T.pack $ show x
   in T.pack (replicate (n - T.length result) c) <> result

showProjects :: (WakaSummary -> [WakaInfo]) -> [WakaSummary] -> [Text]
showProjects f = concatMap (map showProject . f)
  where
    showProject w = fromMaybe "" (wakaInfoName w) <> " " <> T.pack (show (wakaInfoHours w)) <> ":" <> showPad '0' 2 (wakaInfoMinutes w)

cycler :: Applicative f => f Int -> f [Text] -> f Text
cycler counter stream = selector <$> counter <*> stream

enBar :: (Ord key, Dispable key, Dispable value) => (object -> key) -> ([object] -> value) -> [object] -> [Text]
enBar key value = map go . groupOn key . sortOn key
  where
    go x = (display . key . head $ x) <> " " <> (display . value $ x)

myGh :: (LiBarMonad m t Config, Formatter f) => f -> m (Dynamic t [Text])
myGh f = do
  gh <- mkEventReader getGh
  return $ ("\xf408":) . enBar ghNoteReason length <$> gh

effBar :: (Dispable name, Dispable value) => [object -> key] -> (key -> [name]) -> (key -> [value]) -> object -> [[(name, value)]]
effBar effs name value x = map go $ effs
  where
    go key = inner . key $ x
    inner v = zip (name v) (value v)


myWaka :: (LiBarMonad m t Config, Formatter f) => f -> m (Dynamic t [Text])
myWaka f = do
  waka <- mkEventReader getKey
  let keys = [ wakaSummaryProjects, wakaSummaryEditors, wakaSummaryLanguages, wakaSummaryOperating_systems, wakaSummaryMachines]
  let namer = map (wakaInfoName)
  let valuer = map (\x -> (display . wakaInfoHours $ x) <> ":" <> (showPad '0' 2 $ wakaInfoMinutes x))
  return $ concatMap (map (rightBarCycle f . LiBar.Formatter.crop 60) . effBar keys namer valuer) <$> waka

dispUp :: Displayer a -> Displayer [a]
dispUp d = \lst old -> mconcat $ map (flip d old) lst

myCpu :: (LiBarMonad m t Config) => m (Dynamic t Text)
myCpu = do
  cpu <- cpuLoad
  return $ afixBar [dispUp vbar, marked "\xF108"] . map (\x -> (1 - cpuIdle x / cpuSum x)) . tail <$> cpu

myMem :: (LiBarMonad m t Config) => m (Dynamic t Text)
myMem = do
  mem <- mkEventDef $ memUsedFraction <$> memLoad
  return $ afixBar [vbar, marked "\xF2DB"] <$> mem

myNet :: (LiBarMonad m t Config) => m [Dynamic t Text]
myNet = do
  net <- netLoad
  let up = afixBar [vbar, marked "\xF093"] . (/ 7e6) . head . fromMaybe [0] . M.lookup "enp4s0" <$> net
  let down = afixBar [vbar, marked "\xF019"] . (/ 7e6) . (!! 1) . fromMaybe [0, 0] . M.lookup "enp4s0" <$> net
  return $ [up, down]

example :: (LiBarMonad m t Config) => m (Event t Text, Event t ())
example = mdo
  f <- asks formatter
  line <- mkEvent "" T.getLine
  time <- fmap (afixBar [marked "\xF073"]) <$> (mkEvent "" $ getTime "%x %T")
  cpu <- myCpu
  mem <- myMem
  net <- myNet
  counter <- count =<< tickLossyFromPostBuildTime 20
  w <- myWaka f
  gh <- myGh f
  let gh' = rightBarCycle f <$> gh
  let left = cycler counter $ ((: []) <$> gh') <> w
  let eQuit = void . ffilter (== "quit") $ updated line

  let right = afixBar [barRight f] . barCycle f <$> (distributeListOverDyn $ net <> [cpu, mem, time])
  let tagged = tripartite f <$> left <*> line <*> right
  final <- throttle 0.5 $ updated tagged
  return (final, eQuit)

data Config = Config
  { wakaPassword :: Text,
    ghPassword :: Text,
    ghUser :: Text,
    formatter :: LemonBar
  }

myConfig :: IO Config
myConfig = do
  pass <- process "pass wakatime-api"
  let user = "rprospero"
  ghp <- process "pass github_message_token"
  return $ Config pass (T.init ghp) user def

main :: IO ()
main = do
  config <- myConfig
  libar config example
